{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Dask@NERSC: Compute $\\pi$ with Dask Distributed\n",
    "\n",
    "This notebook shows you how to use a Dask cluster to do some parallel processing.\n",
    "It demonstrates map+reduce parallelizing a simple Monte Carlo calculation to estimate the value of $\\pi$.\n",
    "\n",
    "You will learn to:\n",
    "\n",
    "* Connect a client running in your notebook to an existing Dask cluster.\n",
    "* Use `map()` and `submit()` functions to do a simple calculation on the cluster.\n",
    "* Visualize tasks, monitor the cluster, and more with the Dask dashboard.\n",
    "\n",
    "We assume that you already know how to:\n",
    "\n",
    "* Start up a Dask cluster on Cori compute nodes.\n",
    "* Connect to the Dask dashboard (optional, but strongly recommended).\n",
    "\n",
    "**Caveat:** This notebook has been tested with about 300 workers.\n",
    "That's about 10 Haswells.\n",
    "Things may go faster if you run a bigger job.\n",
    "\n",
    "How to do these things and more can be found in our collection of example notebooks."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "***\n",
    "\n",
    "# Connect a Client to a Scheduler\n",
    "\n",
    "We need to talk to the scheduler from our notebook.\n",
    "The `--scheduler-file` argument provided to Dask when it starts gives us a point of coordination with the cluster.\n",
    "We'll start a `Client` object in our notebook that can talk to the remote scheduler.\n",
    "The client reads the content of the scheduler file to find the cluster.\n",
    "Finally we have the cluster restart to clear its state before sending it work."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "\n",
    "import dask\n",
    "from dask.distributed import Client\n",
    "\n",
    "scheduler_file = os.path.join(os.environ[\"SCRATCH\"], \"dask/scheduler.json\")\n",
    "client = Client(scheduler_file=scheduler_file)\n",
    "client.restart()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "***\n",
    "\n",
    "# Map Function\n",
    "\n",
    "To estimate $\\pi$ we will use the [Monte Carlo integration method.](https://en.wikipedia.org/wiki/Monte_Carlo_integration#Example)\n",
    "We will use Dask to parallelize the calculation to maximize our statistics in a limited amount of wall-time.\n",
    "We define a function that takes a random number seed and a `count` argument.\n",
    "This will evenly distribute randomly selected points in the first quadrant of the unit square.\n",
    "The number of points that fall within the unit circle will be used to estimate its area."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "\n",
    "def simulate(seed, count=100):\n",
    "    np.random.seed(seed)\n",
    "    xy = np.random.uniform(size=(count, 2))\n",
    "    return ((xy * xy).sum(1) < 1.0).sum(), count"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Futures\n",
    "\n",
    "We will use 100 billion randomly selected points for our estimate of $\\pi$.\n",
    "We divide these selections up into 10000 groups.\n",
    "We select this size based on the rule of thumb that Dask's scheduling overhead is 1 ms per task.\n",
    "That means we expect about 10 seconds of overhead here.\n",
    "It is recommended to not ask Dask to process a large number of tiny tasks.\n",
    "A smaller number of big tasks works better.\n",
    "\n",
    "We pass our function, a set of seeds (one per group) and the count argument to the `map()` function of the client.\n",
    "This returns a list of futures or promises that represent each task.\n",
    "Computation starts immediately, but results stay on the workers until we ask for them back through the client."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "total = 100000000000\n",
    "tasks = 10000\n",
    "count = total // tasks\n",
    "base_seed = 9876543\n",
    "futures = client.map(simulate, list(base_seed + np.arange(tasks, dtype=int)), count=count)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "***\n",
    "\n",
    "# Reduce\n",
    "\n",
    "If you have the dashboard open you may not see anything for a few seconds while the scheduler is figuring out what to do.\n",
    "The `map()` function doesn't block, and we can start working on the futures as they finish.\n",
    "Let's keep going with a reduce function.\n",
    "\n",
    "This function will take all the futures given to it and after they are completed, combine them to get our estimate of $\\pi$.\n",
    "Assuming they were all complete, we'd take the ratio of hits (points inside the unit circle) to that of all points.\n",
    "Multiplying this by 4 gives us the area of the unit circle, which is of course supposed to be $\\pi$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def reduce(results):\n",
    "    total_hits = 0\n",
    "    total_count = 0\n",
    "    for hits, count in results:\n",
    "        total_hits += hits\n",
    "        total_count += count\n",
    "    return 4.0 * total_hits / total_count"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pi = client.submit(reduce, futures).result()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As this function runs, if you have the dashboard open, you will see something really interesting.\n",
    "As the `simulate()` function runs and results come out of it, the `reduce()` function will start consuming those completed results.\n",
    "Dask has figured out the dependencies between the tasks.\n",
    "\n",
    "Let's take a look at the answer.\n",
    "We got it back using the `result()` function from the future returned by `submit()`.\n",
    "We're in pretty good shape, the relative error is less than 10$^{-6}$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "(pi - np.pi) / np.pi"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "***\n",
    "\n",
    "# Exercises\n",
    "\n",
    "1. Go back and re-execute the cells with the `map()` and `submit()` functions.  What happens?  Why?\n",
    "2. Restart the client, and then repeat the above exercise.  Observe the behavior.\n",
    "3. Now go change the value of `base_seed` used in the `map()` call and re-run the calculation.  What happens and why?\n",
    "4. Try for a trillion (add a zero) trials.  Does it crash?  How can you prevent it from crashing? "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python [conda env:.conda-dask1116a]",
   "language": "python",
   "name": "conda-env-.conda-dask1116a-py"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
