{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Dask@NERSC: DECam Legacy Survey Data with Dask Dataframes\n",
    "\n",
    "This notebook shows you how to use a Dask cluster to visualize a big data set quickly.\n",
    "We use public data from the DECam Legacy Survey to make a heat-map color-color diagram.\n",
    "We sift through over a billion catalogued astrophysical objects to do this in just a couple minutes.\n",
    "\n",
    "You will learn to:\n",
    "\n",
    "* Connect a client running in your notebook to an existing Dask cluster.\n",
    "* Read data from HDF5 files into Dask dataframes.\n",
    "* Repartition and persist data on workers efficiently.\n",
    "* Map a function to partitioned data and combine it to get a result.\n",
    "\n",
    "We assume that you already know how to:\n",
    "\n",
    "* Start up a Dask cluster on Cori compute nodes.\n",
    "* Connect to the Dask dashboard (optional, but highly recommended).\n",
    "\n",
    "**Caveats:**\n",
    "* To use this notebook you need the data.\n",
    "  It's public, but we've pre-converted it into HDF5 with just the data we need to make this demo more fun.\n",
    "  Another notebook showing how to use Dask to do the data conversion from FITS to HDF5 was used, but we're making it better.\n",
    "  It's a useful example of a custom Dask Delayed workflow.\n",
    "* This notebook has been tested with about 300 workers.\n",
    "  That's about 10 Haswells.\n",
    "  Things may go faster if you run a bigger job.\n",
    "\n",
    "**Thanks:** To Matt Rocklin from NVIDIA for his help in optimizing the flow of this notebook."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "***\n",
    "\n",
    "# Connect a Client to a Scheduler\n",
    "\n",
    "We need to talk to the scheduler from our notebook.\n",
    "The `--scheduler-file` argument provided to Dask when it starts gives us a point of coordination with the cluster.\n",
    "We'll start a `Client` object in our notebook that can talk to the remote scheduler.\n",
    "The client reads the content of the scheduler file to find the cluster.\n",
    "Finally we have the cluster restart to clear its state before sending it work."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "\n",
    "import dask\n",
    "from dask.distributed import Client\n",
    "\n",
    "scheduler_file = os.path.join(os.environ[\"SCRATCH\"], \"dask/scheduler.json\")\n",
    "client = Client(scheduler_file=scheduler_file)\n",
    "client.restart()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "***\n",
    "\n",
    "# Read Lots of Data\n",
    "\n",
    "The data set has been preprocessed from its original FITS format into HDF5.\n",
    "We have further selected the columns that are interesting for this analysis.\n",
    "The data conversion process will be covered in another notebook.\n",
    "\n",
    "The data has been stored as a chunked Dask dataframe into about 350 individual HDF5 files.\n",
    "The `chunksize` parameter tells Dask dataframe how to chunk the data into partitions no bigger than that size.\n",
    "The right `chunksize` depends on the problem, but you really don't want a large number of very small chunks.\n",
    "A moderate number of large sized chunks is probably better.\n",
    "\n",
    "**This is why the dashboard is so important.**\n",
    "It's not just entertaining to watch.\n",
    "The dashboard is essential to understand and profile your workflow."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And now to actually read the data into a Dask dataframe with `read_hdf()`.\n",
    "Note that we can pass a glob of files to load up."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import dask.dataframe\n",
    "\n",
    "dd = dask.dataframe.read_hdf(os.path.join(os.environ[\"SCRATCH\"], \"legacysurvey/dr8/north-south-tractor-grz-*.h5\"), \"/data\",\n",
    "                            chunksize=15000000, mode=\"r\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `repartition` function is used to distribute the dataframes over the workers.\n",
    "You don't have to make this match the number of workers at all as we do here.\n",
    "Some other rule of thumb may be optimal.\n",
    "Next we use `persist` to actually persist the data to memory from disk.\n",
    "Things start happening here but it takes a few seconds for them to get started."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dd = dd.repartition(npartitions=len(client.scheduler_info()['workers']))\n",
    "dd = dd.persist()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One thing to notice is that in the cluster dashboard you may see memory and activity on the workers before the task stream updates.\n",
    "This is because the tasks take a little while to load.\n",
    "When they are done they will show up."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "***\n",
    "\n",
    "# Data Science Time\n",
    "\n",
    "The data set consists of astrophysical object fluxes and their inverse variances.\n",
    "We use these data in different passbands to estimate a signal-to-noise for each object.\n",
    "We're using 3 different passbands (g, r, z) and we're going to plot one color (g-r) against another (r-z)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "\n",
    "dd[\"snr_g\"] = dd[\"g\"] * np.sqrt(dd[\"ivar_g\"])\n",
    "dd[\"snr_r\"] = dd[\"r\"] * np.sqrt(dd[\"ivar_r\"])\n",
    "dd[\"snr_z\"] = dd[\"z\"] * np.sqrt(dd[\"ivar_z\"])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here we convert all the fluxes to magnitudes and subtract the magnitudes to get the colors.\n",
    "Sometimes the values in the dataframe don't play nice with the `log10` function.\n",
    "Oh well!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dd[\"g-r\"] = (-2.5*np.log10(dd[\"g\"]) - -2.5*np.log10(dd[\"r\"]))\n",
    "dd[\"r-z\"] = (-2.5*np.log10(dd[\"r\"]) - -2.5*np.log10(dd[\"z\"]))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's use the signal-to-noise ratio to just keep the best data.\n",
    "And while we're at it, let's pay attention to just star-like objects of type \"PSF.\"\n",
    "(The space in the type name is a quirk of the data storage convention.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "thresh = 5.0\n",
    "dd[\"mask\"] = (dd[\"snr_g\"]>thresh) & (dd[\"snr_r\"]>thresh) & (dd[\"snr_z\"]>thresh) & (dd[\"typ\"]==\"PSF \")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Since the persist call, nothing has happened yet.**\n",
    "The above cells just chain together tasks into dependencies.\n",
    "We have to do some kind of computation before they actually execute.\n",
    "That's what we pay the scheduler for: To figure out what order to do this work in, on however many workers we have.\n",
    "\n",
    "Below is the function we'll use to make a kind of heatmap or a 2D histogram represented as an image.\n",
    "We do this manually, binning objects by color along each dimension.\n",
    "We throw away data outside the region of interest `extent`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def heatmap(df, bins, extent):\n",
    "    odx = bins[0] / (extent[1] - extent[0])\n",
    "    ody = bins[1] / (extent[3] - extent[2])\n",
    "    keep = (df[\"g-r\"] >= extent[0]) & (df[\"g-r\"] < extent[1]) & (df[\"r-z\"] >= extent[2]) & (df[\"r-z\"] < extent[3]) & df[\"mask\"]\n",
    "    image = np.zeros(bins)\n",
    "    for i, j in zip((odx * (df[\"g-r\"][keep] - extent[0])).astype(int), (ody * (df[\"r-z\"][keep] - extent[2])).astype(int)):\n",
    "        image[i, j] += 1.0\n",
    "    return (image,)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We'll have each partition run this function against the data stored in its part of the Dask dataframe.\n",
    "To do this, we use the `map_partitions` function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "out = dd.map_partitions(heatmap, (100, 100), (-1.0, 3.0, -1.0, 3.0))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here is where it all comes together.\n",
    "We create an empty image, and we iterate over all the returned images and just sum them up.\n",
    "We don't have to transfer all the data back to the client to make this image.\n",
    "Just the images from each partition are needed.\n",
    "What kicks everything off here is the `compute()` call in the for loop below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "image = np.zeros((100,100))\n",
    "for p in out.compute():\n",
    "    image += p[0]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And finally let's see the image."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.imshow(np.log10(image), origin=\"lower\", extent=(-1.0, 3.0, -1.0, 3.0))\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "***\n",
    "\n",
    "# Exercises\n",
    "\n",
    "1. Go back and re-execute all the cells starting with the data load-up.  What happens?  Why?\n",
    "2. Change the signal-to-noise threshold `thresh` and re-run the calculation.\n",
    "3. Change the binning and/or extent; what cells do you need to re-run to trigger an update of the heat map?\n",
    "4. Re-start the cluster with `client.restart()` and then change the \"*\" in the glob to just \"05?\" so it loads a subset, and rerun."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python [conda env:.conda-dask1116a]",
   "language": "python",
   "name": "conda-env-.conda-dask1116a-py"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
