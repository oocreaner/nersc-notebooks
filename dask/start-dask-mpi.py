
import argparse
import asyncio
import os
import shlex
import shutil
import signal
import subprocess
import textwrap

config = {
    "haswell": {
        "cpus": 32,
        "cores": 2
    },
    "knl": {
        "cpus": 68,
        "cores": 4
    }
}

def main():
    run(parse_args())

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--account", "-A",
            help="Account if not default")
    parser.add_argument("--ntasks", "-n", 
            help="Number of tasks to launch [%(default)d]",
            default=32,
            type=int)
    parser.add_argument("--cpus-per-task", "-c", 
            help="CPUs per task [%(default)d]", 
            default=2,
            type=int)
    parser.add_argument("--time", "-t", 
            help="Time limit in minutes [%(default)d]",
            default=30,
            type=int)
    parser.add_argument("--constraint", "-C", 
            help="Constraint [%(default)s]",
            choices=["haswell", "knl"], 
            default="haswell")
    parser.add_argument("--qos", "-q", 
            help="QOS to submit to [%(default)s]",
            default="interactive")
    parser.add_argument("--image",
            help="Shifter image. If set, requires also --python and --dask-mpi")
    parser.add_argument("--python",
            help="Path to python executable, can be discovered if not using Shifter")
    parser.add_argument("--dask-mpi",
            help="Path to dask-mpi executable, can be discovered if not using Shifter")
    parser.add_argument("--scheduler-file", 
            help="Scheduler file name [%(default)s]",
            default="scheduler.json")
    parser.add_argument("--verbose", 
            help="Print out command to be submitted",
            action="store_true")
    parser.add_argument("--test", 
            help="Print command but don't submit",
            action="store_true")
    parser.add_argument("--confirm", 
            help="Confirm before submission",
            action="store_true")
    args = parser.parse_args()

    if args.image:
        args.python = args.python or "python3"
        args.dask_mpi = args.dask_mpi or "dask-mpi"
    else:
        args.python = args.python or shutil.which("python3")
        args.dask_mpi = args.dask_mpi or shutil.which("dask-mpi")

    if not args.python:
        raise ValueError("Cannot find python executable, see `--python` option")

    if not args.dask_mpi:
        raise ValueError("Cannot find `dask-mpi` executable, see `--dask-mpi` option")

    conf = config[args.constraint]
    ncores = conf["cpus"] * conf["cores"]
    args.nodes, r = divmod(args.ntasks * args.cpus_per_task, ncores)
    if r > 0:
        args.nodes += 1

    return args

def run(args):
    set_omp_num_threads(args)
    clear_old_scheduler_file(args)
    command = format_command(args)
    if args.verbose or args.test or args.confirm:
        print()
        print(f"OMP_NUM_THREADS: {os.environ['OMP_NUM_THREADS']}")
        print(command)
        print()
    if args.test:
        print("Test only, exiting")
        exit()
    if args.confirm and input("Are you sure? (y/n) ") != "y":
        exit()
    command = " ".join(shlex.split(command))
    loop = asyncio.get_event_loop()
    loop.run_until_complete(run_process(command))
    loop.close()

async def run_process(command):
    process = await asyncio.create_subprocess_shell(command)
    await process.communicate()

def set_omp_num_threads(args):
    if os.environ.get("OMP_NUM_THREADS"):
        return
    os.environ["OMP_NUM_THREADS"] = str(omp_num_threads(args))

def omp_num_threads(args):
    return args.cpus_per_task // config[args.constraint]["cores"]

def clear_old_scheduler_file(args):
    try:
        os.remove(args.scheduler_file)
    except:
        pass

def format_command(args):
    if args.account:
        account_option = f"--account={args.account}"
    else:
        account_option = ""
    if args.image:
        image_option = f"--image={args.image}"
        shifter = "shifter"
    else:
        image_option = ""
        shifter = ""
    formatted = textwrap.dedent(f"""
    salloc
        {account_option}
        {image_option}
        --nodes={args.nodes}
        --ntasks={args.ntasks}
        --cpus-per-task={args.cpus_per_task}
        --time={args.time}
        --constraint={args.constraint}
        --qos={args.qos}
        srun -u {shifter}
            {args.python} -u {args.dask_mpi}
                --scheduler-file={args.scheduler_file}
                --dashboard-address=0
                --nthreads=1
                --memory-limit=0
                --no-nanny
                --local-directory=/tmp""")
    return "\n".join([line for line in formatted.split("\n") if line.split()])

if __name__ == "__main__":
    main()
