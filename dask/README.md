
# NERSC Notebooks: Dask

There are a few ways to run Dask at NERSC.

## Best Practices

We assume you want to use Dask at NERSC, at scale, because you have more data to interact with than is convenient with just a few CPUs.
By "at scale" we just mean here:

* You want to use all the cores on a single node to run a Dask cluster, or
* You want to run a multi-node Dask cluster.

Here are what we consider best practices for running Dask at scale at NERSC.

### Run a Cori shared CPU node Jupyter notebook server

Since you want to do interactive analysis, you should use a Jupyter notebook.
There are a few ways you can run Jupyter at NERSC.
Here we recommend you run a Jupyter using a Cori shared CPU node notebook server.
Mainly this is because you will want your notebook to be live for longer than an interactive job allows.
You also don't need to host your notebook on a compute node if you follow our recommendations here.
The cluster will run on one more more compute nodes, but only your Dask client will run in the notebook.

### Run your Dask cluster using `dask-mpi` inside a Slurm job

Running a Dask cluster in a Slurm job allows you to use more resources than is available on a Jupyter shared CPU node.
In principle you can run the Dask cluster in a batch (non-interactive) job, but interactive jobs start up faster.
Queue policy is flexible, and may evolve to allow fast-turnaround batch jobs for starting a Dask cluster.

There are many ways to start a Dask cluster in a job, and we have looked at a few of them.
By far, the best fit we've found at NERSC is using `dask-mpi`.
This is a program that leverages MPI to launch a Dask cluster.
Communication between the Dask scheduler and workers doesn't use MPI, but the benefits of `dask-mpi` are:

* You just use one command to launch the whole cluster within a job.
* The Dask scheduler and dashboard processes are launched at rank 0, workers launch on ranks 1 and up.
* Leveraging MPI to launch the cluster brings up the workers much faster than without `dask-mpi`.

### Use a Shifter image to run both the Jupyter kernel and Slurm job

*The next 2 paragraphs are required reading for Python users at NERSC.
It seems like a lot of background, but understanding these next 2 paragraphs will save you time, sanity, and MPP hours.*

A Dask cluster at scale is a large collection of Python processes.
When you launch a large collection of Python processes, those processes import their dependencies from the file system.
Python's import mechanism, even for a single Python process, involves a large number of file system metadata query operations.
Parallel file systems (GPFS like `/global/cfs` or `/global/homes`, or Lustre like `$SCRATCH`) have a small number of metadata servers
to handle these queries as single source of truth.
A large number of queries coming from a large number of Python processes will bottleneck at the metadata server.
The result is that it can take minutes for a Dask cluster to launch.
Using MPI through `dask-mpi` will mean that this process at least will hit a time out and crash the job.

The solution to this general (i.e. not specific to Dask) problem is to avoid querying the metadata servers excessively.
At NERSC there are two recommended ways to do this.
One is to use `/global/common/software` to deploy software on a file system that is mounted read-only on the compute nodes, with
metadata caching enabled on those compute nodes.
The best recommended solution to the problem is to deploy software to compute nodes using Shifter because all the metadata queries 
will be local the the compute nodes, and further, the software is deployed to fast RAM disk there.
We have observed Python processes launch in jobs using all the KNL nodes using Shifter in less than a minute.
Without using Shifter or similar mitigations the jobs wouldn't even start.

What you should conclude is that you should put `dask-mpi` and its dependencies, along with your workflow dependencies,
into a Shifter image and use that image when you run Dask at scale at NERSC.

Because the Dask ecosystem moves quickly and is very sensitive to mismatched dependency versions, it is also recommended
that you use that same Shifter image as your kernel when using Dask from Jupyter.
This way, your set of dependency libraries and their versions will all match up both in the notebook and in the job.
Things may work if there are library version mismatches, but if a library is present in your notebook environment but 
missing from the job environment where Dask is running, you will probably have serialization errors.

Here's an example.  
Suppose you run a `dask-mpi` cluster as recommended here but you use a kernel based on another Shifter image or conda environment.  
And suppose that your kernel includes `astropy` and in your notebook you send some `astropy` object to the Dask cluster.
If your Dask shifter image doesn't have `astropy` installed, you will see serialization errors.

## `start-dask-mpi.py`

The above best practices summarize a lot of lessons learned from struggling with Dask at NERSC.
To help you implement those best practices, we have created a handy wrapper script you may use to launch a Dask cluster using
`dask-mpi` leveraging the interactive QOS on Cori.

MORE TO COME

